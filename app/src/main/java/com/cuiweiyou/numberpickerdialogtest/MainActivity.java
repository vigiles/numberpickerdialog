package com.cuiweiyou.numberpickerdialogtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.cuiweiyou.numberpickerdialog.NumberPickerDialog;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void test(View v) {
        new NumberPickerDialog(
                this,
                new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        ((TextView) findViewById(R.id.num)).setText(newVal + "");
                    }
                },
                90, // 最大值
                20, // 最小值
                40) // 默认值
                .setCurrentValue(55) // 更新默认值
                .show();
    }
}
