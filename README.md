![NumberPickerDialog](numberpickerdialog.png)
<br/>

#NumberPickerDialog
纯数字（单个）选择器弹窗
原载于[www.gaohaiyan.com/1824.html](http://www.gaohaiyan.com/1824.html)
<br/>

##引入
你可以直接引入Module使用源码。
<br/>
<br/>
也可以通过maven引入。<br/>
<b>1.</b>在project的build.gradle里添加maven仓库依赖
``` groovy
	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```
<b>2.</b>然后在module的build.gradle里引入这个库
``` groovy
	dependencies {
	        compile 'com.github.731942771:NumberPickerDialog:1.0'
	}
```

##使用
``` java
	new NumberPickerDialog(
		this,
		new NumberPicker.OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				((TextView) findViewById(R.id.num)).setText(newVal + "");
			}
		},
		90, // 最大值
		20, // 最小值
		40) // 默认值
		.setCurrentValue(55) // 更新默认值
		.show();
```

U A Welcome.